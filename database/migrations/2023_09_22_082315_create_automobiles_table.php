<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    private string $tableName = 'automobiles';
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create($this->tableName, function (Blueprint $table) {
            $table->id();
            $table->bigInteger('car_id');
            $table->bigInteger('model_id');
            $table->integer('year_of_issue')->nullable()->default(null);
            $table->bigInteger('mileage')->nullable()->default(null);
            // TODO: цвет лучше вынести в отдельную модель и ссылаться на id цвета по аналогии с моделью (маркой) авто
            $table->string('color')->nullable()->default(null);
            $table->softDeletes();
            $table->timestamps();

            $table->index('car_id', $this->tableName.'_car_id_idx');
            $table->index('model_id', $this->tableName.'_model_id_idx');
            $table->foreign('car_id')->on('cars')->references('id');
            $table->foreign('model_id')->on('car_models')->references('id');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists($this->tableName);
    }
};
