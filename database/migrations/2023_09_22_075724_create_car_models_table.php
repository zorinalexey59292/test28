<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    private string $tableName = 'car_models';
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create($this->tableName, function (Blueprint $table) {
            $table->id();
            $table->bigInteger('car_id');
            $table->string('name');
            $table->softDeletes();
            $table->timestamps();

            $table->index('car_id', $this->tableName.'_car_id_idx');
            $table->unique(['car_id', 'name'], $this->tableName.'_id_name_udx');
            $table->foreign('car_id')->on('cars')->references('id');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists($this->tableName);
    }
};
