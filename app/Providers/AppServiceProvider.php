<?php

namespace App\Providers;

use App\Services\CRUD\AutomobileService;
use App\Services\CRUD\AutomobileServiceInterface;
use App\Services\CRUD\CarModelService;
use App\Services\CRUD\CarModelServiceInterface;
use App\Services\CRUD\CarService;
use App\Services\CRUD\CarServiceInterface;
use Illuminate\Support\ServiceProvider;

final class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     */
    public function register(): void
    {
        $this->app->singleton(CarServiceInterface::class, CarService::class);
        $this->app->singleton(CarModelServiceInterface::class, CarModelService::class);
        $this->app->singleton(AutomobileServiceInterface::class, AutomobileService::class);
    }

    /**
     * Bootstrap any application services.
     */
    public function boot(): void
    {
        //
    }
}
