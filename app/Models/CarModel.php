<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasOne;

final class CarModel extends Model
{
    use HasFactory;

    protected $guarded = [];

    public function car(): HasOne
    {
        return $this->hasOne(Car::class, 'id', 'car_id');
    }

    public function automobiles(): HasMany
    {
        return $this->hasMany(Automobile::class, 'model_id', 'id');
    }
}
