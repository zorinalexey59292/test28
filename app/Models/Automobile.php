<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasOne;

final class Automobile extends Model
{
    use HasFactory;

    protected $guarded = [];

    public function model(): HasOne
    {
        return $this->hasOne(CarModel::class, 'id', 'model_id');
    }

    public function car(): HasOne
    {
        return $this->hasOne(Car::class, 'id', 'car_id');
    }
}
