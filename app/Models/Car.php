<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

final class Car extends Model
{
    use HasFactory;

    protected $guarded = [];

    public function models(): HasMany
    {
        return $this->hasMany(CarModel::class, 'car_id', 'id');
    }

    public function model(string $name): CarModel|null
    {
        return $this->hasOne(CarModel::class, 'car_id', 'id')
            ->where('name', $name)->first();
    }
}
