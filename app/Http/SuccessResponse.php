<?php

namespace App\Http;

use Illuminate\Http\JsonResponse;

final class SuccessResponse extends JsonResponse
{
    public static function make(array $data): self
    {
        return new self(
            data: [
                'body' => $data['body'] ?? false,
                'message' => $data['message'] ?? null,
                'error' => false,
                'code' => $data['code'] ?? 200
            ]
        );
    }
}
