<?php

namespace App\Http\Resources;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class AutomobileResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @return array<string, mixed>
     */
    public function toArray(Request $request): array
    {
        return [
            'id' => $this->id,
            'model' => CarModelResource::make($this->model),
            'color' => $this->color,
            'year_of_issue' => $this->year_of_issue,
            'mileage' => $this->mileage,
        ];
    }
}
