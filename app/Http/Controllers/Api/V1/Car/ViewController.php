<?php

namespace App\Http\Controllers\Api\V1\Car;


use App\Http\FailResponse;
use App\Http\Resources\CarResource;
use App\Http\SuccessResponse;
use Illuminate\Http\JsonResponse;

class ViewController extends AbstarctCarController
{
    public function __invoke(string $name): JsonResponse
    {
        if ($car = $this->service->view($name)) {
            return SuccessResponse::make([
                    'body' => CarResource::make($car),
                ]
            );
        }

        return FailResponse::make([
                'message' => trans('car.list.fail', compact('name')),
            ]
        );
    }
}
