<?php

namespace App\Http\Controllers\Api\V1\Car;

use App\Http\FailResponse;
use App\Http\Requests\Car\UpdateRequest;
use App\Http\Resources\CarResource;
use App\Http\SuccessResponse;
use Illuminate\Http\JsonResponse;

class UpdateController extends AbstarctCarController
{
    public function __invoke(UpdateRequest $request, string $car): JsonResponse
    {
        if ($car = $this->service->update($request->validated(), $car)) {
            return SuccessResponse::make([
                    'body' => CarResource::make($car),
                    'message' => trans('car.update.success', ['name' => $car->name]),
                ]
            );
        }

        return FailResponse::make([
                'message' => trans('car.update.fail'),
            ]
        );
    }
}
