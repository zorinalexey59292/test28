<?php

namespace App\Http\Controllers\Api\V1\Car;

use App\Http\FailResponse;
use App\Http\Resources\CarResource;
use App\Http\SuccessResponse;
use Illuminate\Http\JsonResponse;

final class ListController extends AbstarctCarController
{
    public function __invoke(): JsonResponse
    {
        if ($cars = $this->service->list()) {
            return SuccessResponse::make([
                    'body' => CarResource::collection($cars)
                ]
            );
        }


        return FailResponse::make([
                'message' => trans('car.list.null'),
            ]
        );
    }
}
