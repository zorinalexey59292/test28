<?php

namespace App\Http\Controllers\Api\V1\Car;

use App\Http\Controllers\Controller;
use App\Models\Car;
use App\Models\CarModel;
use App\Services\CRUD\CarServiceInterface;

abstract class AbstarctCarController extends Controller
{
    final public function __construct(
        protected readonly CarServiceInterface $service
    )
    {
       Car::with(['models', 'model']);
    }
}
