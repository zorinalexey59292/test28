<?php

namespace App\Http\Controllers\Api\V1\Car;

use App\Http\FailResponse;
use App\Http\Requests\Car\CreateRequest;
use App\Http\Resources\CarResource;
use App\Http\SuccessResponse;
use Illuminate\Http\JsonResponse;

final class CreateController extends AbstarctCarController
{
    public function __invoke(CreateRequest $request): JsonResponse
    {
        if ($car = $this->service->create($request->validated())) {
            return SuccessResponse::make([
                    'body' => CarResource::make($car),
                    'message' => trans('car.create.success', ['name' => $car->name]),
                ]
            );
        }

        return FailResponse::make([
                'message' => trans('car.create.fail'),
            ]
        );
    }
}
