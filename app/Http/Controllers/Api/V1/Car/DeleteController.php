<?php

namespace App\Http\Controllers\Api\V1\Car;

use App\Http\FailResponse;
use App\Http\SuccessResponse;
use Illuminate\Http\JsonResponse;

class DeleteController extends AbstarctCarController
{
    public function __invoke(string $name): JsonResponse
    {
        if ($car = $this->service->delete($name)) {
            return SuccessResponse::make([
                    'body' => $car,
                    'message' => trans('car.delete.success', compact('name')),
                ]
            );
        }

        return FailResponse::make([
                'message' => trans('car.delete.fail'),
            ]
        );
    }
}
