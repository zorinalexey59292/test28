<?php

namespace App\Http\Controllers\Api\V1\Automobile;

use App\Http\FailResponse;
use App\Http\Resources\AutomobileResource;
use App\Http\SuccessResponse;
use Illuminate\Http\JsonResponse;

final class ListController extends AbstaractAutomobileController
{
    public function __invoke(string $car, string $name): JsonResponse
    {
        if ($autos = $this->service->list($car, $name)) {
            return SuccessResponse::make([
                    'body' => AutomobileResource::collection($autos)
                ]
            );
        }


        return FailResponse::make([
                'message' => trans('auto.list.null'),
            ]
        );
    }
}
