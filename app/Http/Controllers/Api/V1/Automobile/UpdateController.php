<?php

namespace App\Http\Controllers\Api\V1\Automobile;

use App\Http\FailResponse;
use App\Http\Requests\Automobile\UpdateRequest;
use App\Http\Resources\AutomobileResource;
use App\Http\SuccessResponse;
use App\Models\Automobile;
use Illuminate\Http\JsonResponse;

final class UpdateController extends AbstaractAutomobileController
{
    public function __invoke(UpdateRequest $request, string $car, string $name, Automobile $automobile): JsonResponse
    {
        if ($auto = $this->service->update($request->validated(), $automobile)) {
            return SuccessResponse::make([
                    'body' => AutomobileResource::make($auto),
                    'message' => trans('car.update.success'),
                ]
            );
        }

        return FailResponse::make([
                'message' => trans('car.update.fail'),
            ]
        );
    }
}
