<?php

namespace App\Http\Controllers\Api\V1\Automobile;

use App\Http\FailResponse;
use App\Http\SuccessResponse;
use App\Models\Automobile;
use Illuminate\Http\JsonResponse;

final class DeleteController extends AbstaractAutomobileController
{
    public function __invoke(string $car, string $name, Automobile $automobile): JsonResponse
    {
        if ($auto = $this->service->delete($automobile)) {
            return SuccessResponse::make([
                    'body' => $auto,
                    'message' => trans('auto.delete.success'),
                ]
            );
        }

        return FailResponse::make([
                'message' => trans('auto.delete.fail'),
            ]
        );
    }
}
