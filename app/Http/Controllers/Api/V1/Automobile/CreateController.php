<?php

namespace App\Http\Controllers\Api\V1\Automobile;

use App\Http\FailResponse;
use App\Http\Requests\Automobile\CreateRequest;
use App\Http\Resources\AutomobileResource;
use App\Http\SuccessResponse;
use Illuminate\Http\JsonResponse;

final class CreateController extends AbstaractAutomobileController
{
    public function __invoke(CreateRequest $request, string $car,string $name): JsonResponse
    {
        if ($auto = $this->service->create($request->validated(),  $car, $name)) {
            return SuccessResponse::make([
                    'body' => AutomobileResource::make($auto),
                    'message' => trans('auto.create.success', ['name' => $auto->name]),
                ]
            );
        }

        return FailResponse::make([
                'message' => trans('auto.create.fail'),
            ]
        );
    }
}
