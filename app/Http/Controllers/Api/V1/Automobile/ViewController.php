<?php

namespace App\Http\Controllers\Api\V1\Automobile;

use App\Http\FailResponse;
use App\Http\Resources\AutomobileResource;
use App\Http\SuccessResponse;
use App\Models\Automobile;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

final class ViewController extends AbstaractAutomobileController
{
    public function __invoke(string $car, string $name, Automobile $automobile): JsonResponse
    {

        if ($auto = $this->service->view($automobile)) {
            return SuccessResponse::make([
                    'body' => AutomobileResource::make($auto),
                ]
            );
        }

        return FailResponse::make([
                'message' => trans('car.list.fail'),
            ]
        );
    }
}
