<?php

namespace App\Http\Controllers\Api\V1\Automobile;

use App\Http\Controllers\Controller;
use App\Models\Car;
use App\Models\CarModel;
use App\Services\CRUD\AutomobileServiceInterface;

abstract class AbstaractAutomobileController extends Controller
{
    final public function __construct(
        protected readonly AutomobileServiceInterface $service
    )
    {
        Car::with(['car']);
        CarModel::with(['model']);
    }
}
