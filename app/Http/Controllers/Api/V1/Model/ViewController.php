<?php

namespace App\Http\Controllers\Api\V1\Model;

use App\Http\FailResponse;
use App\Http\Resources\CarModelResource;
use App\Http\SuccessResponse;
use Illuminate\Http\JsonResponse;

class ViewController extends AbstractCarModelController
{
    public function __invoke(string $car, string $name): JsonResponse
    {
        if ($model = $this->service->view($name, $car)) {
            return SuccessResponse::make([
                    'body' => CarModelResource::make($model),
                ]
            );
        }

        return FailResponse::make([
                'message' => trans('model.list.fail', compact('name')),
            ]
        );
    }
}
