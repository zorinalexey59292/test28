<?php

namespace App\Http\Controllers\Api\V1\Model;

use App\Http\FailResponse;
use App\Http\SuccessResponse;
use Illuminate\Http\JsonResponse;

class DeleteController extends AbstractCarModelController
{
    public function __invoke(string $car, string $name): JsonResponse
    {
        if ($model = $this->service->delete($name, $car)) {
            return SuccessResponse::make([
                    'body' => $model,
                    'message' => trans('model.delete.success', compact('name')),
                ]
            );
        }

        return FailResponse::make([
                'message' => trans('model.delete.fail'),
            ]
        );
    }
}
