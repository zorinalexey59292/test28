<?php

namespace App\Http\Controllers\Api\V1\Model;

use App\Http\FailResponse;
use App\Http\Requests\CarModel\CreateRequest;
use App\Http\Resources\CarModelResource;
use App\Http\SuccessResponse;
use Illuminate\Http\JsonResponse;

final class CreateController extends AbstractCarModelController
{
    public function __invoke(CreateRequest $request, string $car): JsonResponse
    {
        if ($model = $this->service->create($request->validated(), $car)) {
            return SuccessResponse::make([
                    'body' => CarModelResource::make($model),
                    'message' => trans('model.create.success', ['name' => $model->name]),
                ]
            );
        }

        return FailResponse::make([
                'message' => trans('model.create.fail'),
            ]
        );
    }
}
