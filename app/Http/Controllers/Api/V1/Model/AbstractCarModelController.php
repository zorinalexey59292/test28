<?php

namespace App\Http\Controllers\Api\V1\Model;

use App\Http\Controllers\Controller;
use App\Models\CarModel;
use App\Services\CRUD\CarModelServiceInterface;

abstract class AbstractCarModelController extends Controller
{
    final public function __construct(
        protected readonly CarModelServiceInterface $service
    )
    {
        CarModel::with(['car', 'automobiles']);
    }
}
