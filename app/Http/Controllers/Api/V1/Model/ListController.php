<?php

namespace App\Http\Controllers\Api\V1\Model;

use App\Http\FailResponse;
use App\Http\Resources\CarModelResource;
use App\Http\SuccessResponse;
use Illuminate\Http\JsonResponse;

final class ListController extends AbstractCarModelController
{
    public function __invoke(string $car): JsonResponse
    {
        if ($models = $this->service->list($car)) {
            return SuccessResponse::make([
                    'body' => CarModelResource::collection($models)
                ]
            );
        }

        return FailResponse::make([
                'message' => trans('model.list.fail', ['name' => $car]),
            ]
        );
    }
}
