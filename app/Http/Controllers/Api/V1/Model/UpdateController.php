<?php

namespace App\Http\Controllers\Api\V1\Model;

use App\Http\FailResponse;
use App\Http\Requests\CarModel\UpdateRequest;
use App\Http\Resources\CarModelResource;
use App\Http\SuccessResponse;
use Illuminate\Http\JsonResponse;

class UpdateController extends AbstractCarModelController
{
    public function __invoke(UpdateRequest $request, string $car, string $name): JsonResponse
    {
        if ($model = $this->service->update($request->validated(), $car, $name)) {
            return SuccessResponse::make([
                    'body' => CarModelResource::make($model),
                    'message' => trans('model.update.success', ['name' => $model->name]),
                ]
            );
        }

        return FailResponse::make([
                'message' => trans('model.update.fail'),
            ]
        );
    }
}
