<?php

namespace App\Http;

use Illuminate\Http\JsonResponse;

final class FailResponse extends JsonResponse
{
    public static function make(array $data): self
    {
        return new self(
            data: [
                'body' => $data['body'] ?? false,
                'message' => $data['message'] ?? null,
                'error' => true,
                'code' => $data['code'] ?? 500
            ]
        );
    }
}
