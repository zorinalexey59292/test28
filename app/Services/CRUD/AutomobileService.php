<?php

namespace App\Services\CRUD;

use App\Models\Automobile;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

final class AutomobileService implements AutomobileServiceInterface
{
    public function __construct(
        private readonly CarModelServiceInterface $model,
    )
    {
    }

    public function create(array $data, string $car, string $name): Builder|Model|bool
    {
        try {
            if($model = $this->model->view($name, $car)){
                $data['car_id'] = $model->car->id;
                $data['model_id'] = $model->id;

                return Automobile::query()->create($data);
            }
        }catch (\Exception $e){

        }

        return false;
    }

    public function list(string $car, string $name): Collection|null
    {
        if($model = $this->model->view($name, $car)){
            return  $model->automobiles;
        }

        return null;
    }

    public function view(Automobile $auto): Automobile|null
    {
        return $auto;
    }

    public function update(array $data, Automobile $auto): Automobile|bool
    {
        foreach ($data as $item => $value){
            $auto->$item = $value;
        }

        if($auto->save()){
            return  $auto;
        }

        return false;
    }

    public function delete(Automobile $auto): bool
    {
        return $auto->delete();
    }
}
