<?php

namespace App\Services\CRUD;

use App\Models\CarModel;
use Exception;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

final class CarModelService implements CarModelServiceInterface
{
    public function __construct(
        private readonly CarServiceInterface $service
    )
    {
    }

    public function create(array $data, string $car): Builder|Model|bool
    {
        try {
            if ($car = $this->service->view($car)) {
                $data['car_id'] = $car->id;

                return CarModel::query()->create($data);
            }
        } catch (Exception $e) {

        }

        return false;
    }

    public function view(string $name, string $car): Model|Builder|null
    {

        if ($car = $this->service->view($car)) {
            return $car->model($name);
        }

        return null;
    }

    public function list(string $car): Collection|null
    {
        if ($car = $this->service->view($car)) {
            return $car->models;
        }

        return null;
    }

    public function update(array $data, string $car, string $name): Builder|Model|bool|null
    {
        if (($car = $this->service->view($car)) && ($model = $car->model($name)) && $model->update($data)) {
            return $model;
        }

        return null;
    }

    public function delete(string $name, string $car): bool
    {
        return (($car = $this->service->view($car)) && ($model = $car->model($name)) && $model->delete());
    }
}
