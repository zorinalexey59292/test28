<?php

namespace App\Services\CRUD;

use App\Models\Car;
use App\Models\CarModel;
use Exception;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

final class CarService implements CarServiceInterface
{
    public function create(array $data): Builder|Model|bool
    {
        try {
            return Car::query()->create($data);
        } catch (Exception $e) {

        }

        return false;
    }

    public function list(): Collection|array
    {
        return Car::query()->get();
    }

    public function view(string $name): Model|Builder|null
    {
        return Car::query()->where('name', $name)->first();
    }

    public function update(array $data, string $car): Builder|Model|bool|null
    {
        try {
            if (($car = Car::query()->where('name', $car)->first()) && $car->update(['name' => $data['name']])) {
                return $car;
            }
        } catch (Exception $e) {

        }

        return false;
    }

    public function delete(string $name): bool
    {
        try {
            if (($car = Car::query()->where('name', $name)->first()) && $car->delete()) {
                return true;
            }
        } catch (Exception $e) {

        }

        return false;
    }
}
