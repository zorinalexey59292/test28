<?php

namespace App\Services\CRUD;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

interface CarModelServiceInterface
{
    public function create(array $data, string $car): Builder|Model|bool;

    public function list(string $car): Collection|null;

    public function view(string $car, string $name): Model|Builder|null;

    public function update(array $data, string $car, string $name): Builder|Model|bool|null;

    public function delete(string $car, string $name): bool;
}
