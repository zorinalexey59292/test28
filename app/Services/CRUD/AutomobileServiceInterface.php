<?php

namespace App\Services\CRUD;

use App\Models\Automobile;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

interface AutomobileServiceInterface
{
    public function create(array $data, string $car, string $name): Builder|Model|bool;

    public function list(string $car, string $name): Collection|null;

    public function view(Automobile $auto): Automobile|null;

    public function update(array $data, Automobile $auto): Automobile|bool;

    public function delete(Automobile $auto): bool;
}
