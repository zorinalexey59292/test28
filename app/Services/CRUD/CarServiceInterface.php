<?php

namespace App\Services\CRUD;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

interface CarServiceInterface
{
    public function create(array $data): Builder|Model|bool;

    public function list(): Collection|array;

    public function view(string $name): Model|Builder|null;

    public function update(array $data, string $car): Builder|Model|bool|null;

    public function delete(string $name): bool;
}
