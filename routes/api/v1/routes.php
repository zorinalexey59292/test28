<?php

use App\Http\Controllers\Api\V1\Automobile\CreateController as AutoCreateController;
use App\Http\Controllers\Api\V1\Automobile\DeleteController as AutoDeleteController;
use App\Http\Controllers\Api\V1\Automobile\ListController as AutoListController;
use App\Http\Controllers\Api\V1\Automobile\UpdateController as AutoUpdateController;
use App\Http\Controllers\Api\V1\Automobile\ViewController as AutoViewController;
use App\Http\Controllers\Api\V1\Car\CreateController as CarCreateController;
use App\Http\Controllers\Api\V1\Car\DeleteController as CarDeleteController;
use App\Http\Controllers\Api\V1\Car\ListController as CarListController;
use App\Http\Controllers\Api\V1\Car\UpdateController as CarUpdateController;
use App\Http\Controllers\Api\V1\Car\ViewController as CarViewController;
use App\Http\Controllers\Api\V1\Model\CreateController as ModelCreateController;
use App\Http\Controllers\Api\V1\Model\DeleteController as ModelDeleteController;
use App\Http\Controllers\Api\V1\Model\ListController as ModelListController;
use App\Http\Controllers\Api\V1\Model\UpdateController as ModelUpdateController;
use App\Http\Controllers\Api\V1\Model\ViewController as ModelViewController;
use App\Models\Automobile;
use Illuminate\Support\Facades\Route;

Route::middleware('api')->group(static function () {

    Route::prefix('car')->name('car.')->group(static function () {
        Route::get('', CarListController::class)->name('list');
        Route::get('{name}', CarViewController::class)->name('view');
        Route::post('', CarCreateController::class)->name('create');
        Route::patch('{name}', CarUpdateController::class)->name('update');
        Route::delete('{name}', CarDeleteController::class)->name('delete');
    });

    Route::prefix('{car}')->name('car_model.')->group(static function () {
        Route::get('', ModelListController::class)->name('list');
        Route::get('{name}', ModelViewController::class)->name('view');
        Route::post('', ModelCreateController::class)->name('create');
        Route::patch('{name}', ModelUpdateController::class)->name('update');
        Route::delete('{name}', ModelDeleteController::class)->name('delete');
    });

    Route::prefix('auto/{car}/{name}')->name('auto.')->group(static function () {
        Route::get('', AutoListController::class)->name('list');
        Route::get('{automobile}', AutoViewController::class)->name('view');
        Route::post('', AutoCreateController::class)->name('create');
        Route::patch('{automobile}', AutoUpdateController::class)->name('update');
        Route::delete('{automobile}', AutoDeleteController::class)->name('delete');
    });

});
